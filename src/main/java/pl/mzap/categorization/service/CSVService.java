package pl.mzap.categorization.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.mzap.categorization.csv.provider.CSVUtils;
import pl.mzap.categorization.dao.CategoryRepository;
import pl.mzap.categorization.dao.PointRepository;
import pl.mzap.categorization.dao.WordRepository;
import pl.mzap.categorization.model.Category;
import pl.mzap.categorization.model.Point;
import pl.mzap.categorization.model.Word;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CSVService {

    private static final String FILE_PATH = "./";
    private static final String FILE_NAME = "categories.csv";
    private static final String CSV_SEPARATOR = ",";

    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    WordRepository wordRepository;
    @Autowired
    PointRepository pointRepository;

    public void saveCSVFile() {
        try (FileWriter writer = new FileWriter(FILE_PATH + FILE_NAME)) {
            parseCategoriesIntoFileFormat(writer);
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void readCSVFile() {
        try (BufferedReader br = new BufferedReader(new FileReader(FILE_PATH + FILE_NAME))) {
            parseCategoriesFromFile(br);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void parseCategoriesFromFile(BufferedReader br) throws IOException {
        String line;
        while ((line = br.readLine()) != null) {
            String[] values = line.split(CSV_SEPARATOR);
            String categoryNumber = values[0], wordName = values[1], pointRating = values[2];
            createAndSave(categoryNumber, wordName, pointRating);
        }
    }

    private void createAndSave(String categoryNumber, String wordName, String pointRating) {
        Category category = isCategoryExist(categoryNumber);
        Word word = isWordExist(wordName);
        Point point = new Point();
        point.setRating(Integer.parseInt(pointRating));
        word.addPoint(point);
        category.addWord(word);
        categoryRepository.save(category);
    }

    private Word isWordExist(String wordName) {
        Optional<Word> word = wordRepository.findWordByName(wordName);
        return word.orElseGet(() -> new Word(wordName));
    }

    private Category isCategoryExist(String categoryNumber) {
        Optional<Category> category = categoryRepository.findCategoryByNumber(Integer.parseInt(categoryNumber));
        return category.orElseGet(() -> new Category(Integer.parseInt(categoryNumber)));
    }

    private void parseCategoriesIntoFileFormat(FileWriter writer) throws IOException {
        List<Category> categories = categoryRepository.findAll();
        List<String> parsedCategories = new ArrayList<>();

//        CSVUtils.writeLine(writer, Arrays.asList("CAT_NUMBER", "WORD_NAME", "POINT_RATING"));

        for (Category cat : categories) {
            for (Word word : cat.getWords()) {
                for (Point point : word.getPoints()) {
                    parsedCategories.clear();
                    parsedCategories.add(String.valueOf(cat.getNumber()));
                    parsedCategories.add(String.valueOf(word.getName()));
                    parsedCategories.add(String.valueOf(point.getRating()));
                    writeLineIntoFile(writer, parsedCategories);
                }
            }
        }

    }

    private void writeLineIntoFile(FileWriter writer, List<String> parsedCategories) throws IOException {
        CSVUtils.writeLine(writer, parsedCategories);
    }

}
