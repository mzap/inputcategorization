package pl.mzap.categorization.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import pl.mzap.categorization.dao.CategoryRepository;
import pl.mzap.categorization.dao.WordRepository;
import pl.mzap.categorization.mapper.EntityMapper;
import pl.mzap.categorization.model.Word;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class WordService {

    @Autowired
    WordRepository wordRepository;
    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    EntityMapper<Word> mapper;

    public void addWord(Word word) {
        wordRepository.save(word);
    }

    public ResponseEntity<?> addWordToCategory(Word word, int categoryNumber) {
        return categoryRepository.findCategoryByNumber(categoryNumber)
                .map(cat -> {
                    cat.addWord(word);
                    categoryRepository.save(cat);

                    return new ResponseEntity<>(cat.getNumber(), HttpStatus.CREATED);
                })
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    public String getAll() {
        List<Word> words = wordRepository.findAll();
        return mapper.toJson(words);
    }

    public ResponseEntity<?> getWordBy(String name) {
        return wordRepository.findWordByName(name)
                .map(word -> new ResponseEntity<>(mapper.toJson(word), HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_MODIFIED));
    }

    public ResponseEntity<?> updateWordBy(String wordName, Word updatedWord) {
        return wordRepository.findWordByName(wordName)
                .map(word -> {
                    word.setName(updatedWord.getName());
                    word.setPoints(updatedWord.getPoints());
                    wordRepository.save(word);

                    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
                })
                .orElse(new ResponseEntity<>(HttpStatus.NOT_MODIFIED));
    }

    public void deleteWordByName(String name) {
        wordRepository.deleteWordByName(name);
    }
}
