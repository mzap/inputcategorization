package pl.mzap.categorization.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import pl.mzap.categorization.dao.CategoryRepository;
import pl.mzap.categorization.mapper.EntityMapper;
import pl.mzap.categorization.model.Category;
import pl.mzap.categorization.model.Point;
import pl.mzap.categorization.model.Word;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class CategoryService {

    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    EntityMapper<Category> mapper;

    public ResponseEntity<?> addCategory(Category category) {
        return categoryRepository.findCategoryByNumber(category.getNumber())
                .map(cat -> new ResponseEntity<>(HttpStatus.NOT_MODIFIED))
                .orElseGet(() -> {
                    categoryRepository.save(category);

                    return new ResponseEntity<>(HttpStatus.CREATED);
                });
    }

    public String getAll() {
        List<Category> categories = categoryRepository.findAll();
        return mapper.toJson(categories);
    }

    public ResponseEntity<?> getCategoryBy(int categoryNumber) {
        return categoryRepository.findCategoryByNumber(categoryNumber)
                .map(cat -> new ResponseEntity<>(mapper.toJson(cat), HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    public ResponseEntity<?> updateCategory(int categoryNumber, Category newCategory) {
        return categoryRepository.findCategoryByNumber(categoryNumber)
                .map(categoryToUpdate -> {
                    categoryToUpdate.setNumber(newCategory.getNumber());
                    categoryToUpdate.setWords(newCategory.getWords());
                    categoryRepository.save(categoryToUpdate);

                    return new ResponseEntity<>(categoryToUpdate.getNumber(), HttpStatus.OK);
                })
                .orElse(new ResponseEntity<>(HttpStatus.NOT_MODIFIED));
    }

    public void deleteCategoryBy(int categoryNumber) {
        categoryRepository.deleteCategoryByNumber(categoryNumber);
    }

    public String calculateBestCategory() {
        List<Category> categories = categoryRepository.findAll();

        int max = 0, bestNumberCategory = -1;
        for (Category category : categories) {

            int wordsSum = 0;
            int pointsSum = 0;
            for (Word word : category.getWords()) {
                pointsSum += word.getPoints().stream().parallel()
                        .map(Point::getRating)
                        .mapToInt(Integer::intValue)
                        .sum();
            }
            wordsSum += pointsSum;
            if (wordsSum > max) {
                max = wordsSum;
                bestNumberCategory = category.getNumber();
            }
        }
        return "THE BEST CATEGORY WITH ID: " + bestNumberCategory + " WITH " + max + " POINTS";
    }

}
