package pl.mzap.categorization.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.mzap.categorization.dao.CategoryRepository;
import pl.mzap.categorization.dao.PointRepository;
import pl.mzap.categorization.dao.WordRepository;
import pl.mzap.categorization.model.Category;
import pl.mzap.categorization.model.Point;
import pl.mzap.categorization.model.Word;

import java.util.*;

@Service
public class GeneratorService {

    private static final String CHARS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final int RANDOM_NAME_LENGHT = 5;

    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    WordRepository wordRepository;
    @Autowired
    PointRepository pointRepository;

    public void generateRandomData(int categorySize, int wordsInCategory, int pointsInWord){
        for (int i = 0; i < categorySize; i++) {
            Category category = new Category();
            category.setNumber(i);

            Set<Word> words = new HashSet<>();

            for (int j = 0; j < wordsInCategory; j++) {
                Word word = new Word();
                word.setName(generateWordName());

                List<Point> points = new ArrayList<>();

                for (int x = 0; x < pointsInWord; x++) {
                    Point point = new Point();
                    point.setRating(new Random().nextInt(10) + 1);
                    pointRepository.save(point);
                    points.add(point);
                }

                word.setPoints(points);
                wordRepository.save(word);
                words.add(word);
            }
            category.setWords(words);
            categoryRepository.save(category);
        }
    }

    private String generateWordName() {
        StringBuilder randomName = new StringBuilder();

        for (int i = 0; i < RANDOM_NAME_LENGHT; i++) {
            int number = new Random().nextInt(CHARS.length());
            char ch = CHARS.charAt(number);
            randomName.append(ch);
        }
        return randomName.toString();
    }

}
