package pl.mzap.categorization.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import pl.mzap.categorization.dao.PointRepository;
import pl.mzap.categorization.mapper.EntityMapper;
import pl.mzap.categorization.model.Point;

import java.util.List;

@Service
public class PointService {

    @Autowired
    PointRepository pointRepository;
    @Autowired
    EntityMapper<Point> mapper;

    public void addPoint(int rating) {
        Point point = new Point();
        point.setRating(rating);
        pointRepository.save(point);
    }

    public String getAll() {
        List<Point> points = pointRepository.findAll();
        return mapper.toJson(points);
    }

    public ResponseEntity<?> getPointBy(Long pointID) {
        return pointRepository.findById(pointID)
                .map(point -> new ResponseEntity<>(mapper.toJson(point), HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    public ResponseEntity<?> updatePoint(Long pointID, int rating) {
        return pointRepository.findById(pointID)
                .map(pointToUpdate -> {
                    pointToUpdate.setRating(rating);
                    pointRepository.save(pointToUpdate);

                    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
                })
                .orElse(new ResponseEntity<>(HttpStatus.NOT_MODIFIED));
    }

    public void deletePointBy(Long pointID) {
        pointRepository.deleteById(pointID);
    }
}
