package pl.mzap.categorization.controller;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.mzap.categorization.dao.CategoryRepository;
import pl.mzap.categorization.service.GeneratorService;

@Api(value = "Generator", description = "Endpoint which provide generate random data and save it into database")
@RestController
public class GeneratorController {

    @Autowired
    GeneratorService generatorService;
    @Autowired
    CategoryRepository categoryRepository;

    @GetMapping("/generate/{categorySize}/{wordsInCategory}/{pointsInWord}")
    public void generateData(@PathVariable("categorySize") int categorySize,
                             @PathVariable("wordsInCategory") int wordsInCategory,
                             @PathVariable("pointsInWord") int pointsInWord) {
        generatorService.generateRandomData(categorySize, wordsInCategory, pointsInWord);
    }

    @DeleteMapping("/generate/truncate")
    public void truncateDatabase(){
        categoryRepository.deleteAll();
    }


}
