package pl.mzap.categorization.controller;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pl.mzap.categorization.service.CategoryService;

@Api(value = "calculations", description = "REST Controller that share calculations based on categories. Here is the algorithm in reference to the topic")
@RestController
public class CalculationsController {

    @Autowired
    CategoryService categoryService;

    @RequestMapping(value = "/categories/best", method = RequestMethod.GET)
    public String calculateBestCategory() {
        return categoryService.calculateBestCategory();
    }
}
