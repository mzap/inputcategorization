package pl.mzap.categorization.controller;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.mzap.categorization.service.CSVService;

@Api(value = "CSV providers", description = "Endpoints which are provide to save or read csv file")
@RestController
public class CSVController {

    @Autowired
    CSVService csvService;

    @GetMapping("/csv/read")
    public void readCSVFile() {
        csvService.readCSVFile();
    }

    @GetMapping("/csv/save")
    public void saveCSVFile() {
        csvService.saveCSVFile();
    }

}
