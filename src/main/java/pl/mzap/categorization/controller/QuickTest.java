package pl.mzap.categorization.controller;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "quickTest", description = "quick test for other altar apps")
@RestController
public class QuickTest {

  @GetMapping("/test")
  public String test(){
    return "working";
  }

}
