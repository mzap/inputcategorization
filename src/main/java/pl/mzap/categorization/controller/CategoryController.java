package pl.mzap.categorization.controller;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.mzap.categorization.model.Category;
import pl.mzap.categorization.service.CategoryService;

@Api(value = "category", description = "REST Controller that share CRUD for external GUI")
@RestController
public class CategoryController {

    @Autowired
    CategoryService categoryService;

    @PostMapping(value = "/categories", consumes = "application/json")
    public ResponseEntity<?> addCategory(@RequestBody Category category) {
        return categoryService.addCategory(category);
    }

    @GetMapping(value = "/categories", produces = "application/json")
    public String getAllCategories() {
        return categoryService.getAll();
    }

    @GetMapping(value = "/categories/{categoryNumber}", produces = "application/json")
    public ResponseEntity getCategoryByNumber(@PathVariable("categoryNumber") int categoryNumber) {
        return categoryService.getCategoryBy(categoryNumber);
    }

    @PutMapping("/categories/{categoryNumber}")
    public ResponseEntity<?> updateCategory(@PathVariable("categoryNumber") int categoryNumber,
                                            @RequestBody Category category) {
        return categoryService.updateCategory(categoryNumber, category);
    }

    @DeleteMapping("/categories/{categoryNumber}")
    public void deleteCategory(@PathVariable("categoryNumber") int categoryNumber) {
        categoryService.deleteCategoryBy(categoryNumber);

    }

}