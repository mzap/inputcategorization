package pl.mzap.categorization.controller;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.mzap.categorization.model.Word;
import pl.mzap.categorization.service.WordService;

@Api(value = "word", description = "REST Controller that share CRUD for external GUI")
@RestController
public class WordController {

    @Autowired
    WordService wordService;

    @PostMapping("/words")
    public void addWord(@RequestBody Word word) {
        wordService.addWord(word);
    }

    @PostMapping("/words/category/{categoryNumber}")
    public ResponseEntity addWord(@RequestBody Word word,
                                  @PathVariable("categoryNumber") int categoryNumber) {
        return wordService.addWordToCategory(word, categoryNumber);
    }

    @GetMapping("/words")
    public String getAllWords() {
        return wordService.getAll();
    }

    @GetMapping("/words/{name}")
    public ResponseEntity getWordByName(@PathVariable("name") String name) {
        return wordService.getWordBy(name);
    }

    @PutMapping("/words/{name}")
    public ResponseEntity updateWordByName(@PathVariable("name") String wordName,
                           @RequestBody Word word) {
        return wordService.updateWordBy(wordName, word);
    }

    @DeleteMapping("/words/{name}")
    public void deleteWordByName(@PathVariable("name") String name) {
        wordService.deleteWordByName(name);
    }

}
