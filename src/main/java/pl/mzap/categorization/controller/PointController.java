package pl.mzap.categorization.controller;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.mzap.categorization.service.PointService;

@Api(value = "point", description = "REST Controller that share CRUD for external GUI")
@RestController
public class PointController {

    @Autowired
    PointService pointService;

    @PostMapping("/points")
    public void addPoint(@RequestParam("rating") int rating) {
        pointService.addPoint(rating);
    }

    @GetMapping("/points")
    public String getAllPoints() {
        return pointService.getAll();
    }

    @GetMapping("/points/{pointID}")
    public ResponseEntity getPointById(@PathVariable("pointID") Long pointID) {
        return pointService.getPointBy(pointID);
    }

    @PutMapping("/points/{pointID}")
    public ResponseEntity updatePoint(@PathVariable("pointID") Long pointID,
                                      @RequestParam("rating") int rating) {
        return pointService.updatePoint(pointID, rating);
    }

    @DeleteMapping("/points/{pointID}")
    public void deletePoint(@PathVariable("pointID") Long pointID) {
        pointService.deletePointBy(pointID);

    }

}
