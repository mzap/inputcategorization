package pl.mzap.categorization.model;

import com.google.gson.annotations.Expose;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Word {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Expose
    @Getter @Setter
    private String name;
    @Expose
    @Getter @Setter
    @OneToMany
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private List<Point> points = new ArrayList<>();

    public Word(String name) {
        this.name = name;
    }

    public void addPoint(Point point){
        points.add(point);
    }

}
