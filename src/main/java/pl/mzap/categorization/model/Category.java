package pl.mzap.categorization.model;

import com.google.gson.annotations.Expose;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Expose
    @Getter @Setter
    private int number;
    @Expose
    @Getter @Setter
    @OneToMany
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private Set<Word> words = new HashSet<>();

    public Category(int number) {
        this.number = number;
    }

    public void addWord(Word word) {
        words.add(word);
    }
}
