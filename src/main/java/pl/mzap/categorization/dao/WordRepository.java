package pl.mzap.categorization.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.mzap.categorization.model.Word;

import java.util.List;
import java.util.Optional;

@Repository
public interface WordRepository extends CrudRepository<Word, Long> {

    List<Word> findAll();
    Optional<Word> findWordByName(String name);
    void deleteWordByName(String name);

}