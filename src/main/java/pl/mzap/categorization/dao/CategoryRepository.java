package pl.mzap.categorization.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.mzap.categorization.model.Category;

import java.util.List;
import java.util.Optional;

@Repository
public interface CategoryRepository extends CrudRepository<Category, Long> {

    List<Category> findAll();
    Optional<Category> findCategoryByNumber(int number);
    void deleteCategoryByNumber(int categoryNumber);

}
