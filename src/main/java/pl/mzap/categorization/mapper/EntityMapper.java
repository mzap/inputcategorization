package pl.mzap.categorization.mapper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class EntityMapper<T> {

    private static final GsonBuilder gsonBuilder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();
    private static final Gson gson = gsonBuilder.create();

    public String toJson(T object) {
        return gson.toJson(object);
    }

    public String toJson(List<T> objects) {
        return gson.toJson(objects);
    }

}
