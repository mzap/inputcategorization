## INPUT CATEGORIZATION
## REST API

1. Using *Spring-Boot, Spring-Data*
1. Using */api/* context path
2. Using in-memory H2 database
3. Using Gson to parse response
4. Available export to CSV file and import from CSV file
5. Available to choose category where sum points of words in each category has highest value

---

## Database

Application has embedded H2 in-memory SQL database.

1. Go to [database configuration](http://localhost:8080/api/h2)
2. Set **JDBC URL:** to **jdbc:h2:mem:~/input_categorization**
3. Username and password: **mzap**, **mzap**

---

## Swagger UI

Application use swagger for API documentation. -> [go to swagger](http://localhost:8080/api/swagger-ui.html#/) 
